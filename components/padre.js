Vue.component("padre", {
  template: `
    <div class="p-5 bg-dark text-white">
      <p>Número padre: {{numeroPadre}}</p>
      <button class="btn btn-danger" v-on:click="numeroPadre++">+</button>
      <br />
      <p>Nombre que viene del hijo: {{nombrePadre}}</p>
      <br />

      <!--hijo v-bind:numero="numeroPadre" v-on:pasandoNombreHijo="recibeNombre"></hijo-->
      <hijo v-bind:numero="numeroPadre" v-on:pasandoNombreHijo="nombrePadre = $event"></hijo>
      
    </div>
  `,
  data() {
    return {
      numeroPadre: 0,
      nombrePadre: "",
    };
  },
  // methods: {
  //   recibeNombre(nombre) {
  //     this.nombrePadre = nombre;
  //   },
  // },
});
