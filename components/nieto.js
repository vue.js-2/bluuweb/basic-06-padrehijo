Vue.component("nieto", {
  template: `
    <div class="p-5 bg-warning text-black">
        <p><b>Nombre nieto:</b> {{nombreNieto}}</p>
        <button v-on:click="emitirNombreNieto">eNombreNieto</button>
        <br>
        <p>Nombre viene del hijo: {{nombreHijo}}</p>
    </div>
    `,
  props: ["nombreHijo"],
  data() {
    return {
      nombreNieto: "Miguelito",
    };
  },
  methods: {
    emitirNombreNieto() {
      this.$emit("eNombreNieto", this.nombreNieto);
    },
  },
});
