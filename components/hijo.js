Vue.component("hijo", {
  template: `
    <div class="p-5 bg-primary text-white">
      <p>Texto del hijo</p>
      <h5>Número padre: {{numero}}</h5>
      <button v-on:click="numero++">+</button>
      <br>
      <h5>Nombre Hijo: {{nombreHijo}}</h5>
      <br>
      <p> Recibiendo nombre nieto: {{nombreNieto}}</p>
      <nieto v-bind:nombreHijo="nombreHijo" v-on:eNombreNieto="rNombreNieto"></nieto>
      
      </div>  
  `,
  props: ["numero"],
  data() {
    return {
      nombreHijo: "Aurelio",
      nombreNieto: "",
    };
  },
  methods: {
    rNombreNieto(nNieto) {
      this.nombreNieto = nNieto;
    },
  },
  mounted() {
    this.$emit("pasandoNombreHijo", this.nombreHijo);
  },
});
